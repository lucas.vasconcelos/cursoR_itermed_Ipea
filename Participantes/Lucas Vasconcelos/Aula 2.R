# Curso R - Aula 2 --------------------------------------------------------

# Lucas Vasconcelos
# Criei a sub-pasta "Lucas Vasconcelos" em "Participantes"
# 21 de setembro de 2017


# Issue 1 -----------------------------------------------------------------

numero <- rnorm(1000)

# Issue 2 -----------------------------------------------------------------

base.df <- read.table("C:/Users/Lucas/ownCloud/17 Curso Data Science II/cursoR_itermed_Ipea/Participantes/Lucas Vasconcelos/Base.txt",sep="&",header=T)
base.df <- read.table("Participantes/Lucas Vasconcelos/Base.txt",sep="&",header=T)
base.df <- read.table(file.choose(),sep="&",header=T)

# Issue 3 -----------------------------------------------------------------

install.packages("devtools")
devtools::install_github("lucasmation/microdadosBrasil")