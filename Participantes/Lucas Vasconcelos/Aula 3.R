# Curso Ipea - Aula 3 -----------------------------------------------------

# Lucas Vasconcelos
# 27 de setembro de 2017

# Exemplo histograma ------------------------------------------------------

dados <- rnorm(1000,10,3)
hist(dados)

# Instala o pacote microdadosBrasil ---------------------------------------

install.packages("devtools")
devtools::install_github("lucasmation/microdadosBrasil")


# Exercício 1 -------------------------------------------------------------

library(readr)

dic <- readr::read_csv("bancos/dicionario_pessoas.csv")
# dic <- readr::read_csv(file.choose())

# Exercício 2 -------------------------------------------------------------

widths <- readr::fwf_widths(widths = dic$tamanho2,
                            col_names = dic$cod2)
str(widths)

pnad09.df <- readr::read_fwf(file = "bancos/AMOSTRA_DF_PNAD2009p.txt",
                             col_positions = widths)
str(pnad09.df)

# Exercício 3 -------------------------------------------------------------

pnad09.2.df <- microdadosBrasil::read_PNAD(ft = "pessoas",
                                           i = 2009,
                                           file = "bancos/AMOSTRA_DF_PNAD2009p.txt")

# Exercício 4.1 -----------------------------------------------------------

install.packages("readxl")
readxl::excel_sheets("bancos/datasets.xls")

# Exercício 4.2 -----------------------------------------------------------

chickwts.df <- readxl::read_excel(path = "bancos/datasets.xls",
                                  sheet = 3)

# Exercício 4.3 -----------------------------------------------------------

files <- "bancos/datasets.xls"
planilhas <- lapply(readxl::excel_sheets(files),
                    function(x) readxl::read_excel(files, sheet = x))
str(planilhas)

iris <- planilhas[[1]]
mtcars <- planilhas[[2]]
chickwts <- planilhas[[3]]
quakes <- planilhas[[4]]