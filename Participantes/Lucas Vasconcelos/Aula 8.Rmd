---
title: "Aula 8"
author: "Lucas Vasconcelos"
date: "17 de outubro de 2017"
output: word_document
---

# Desafio da aula passada

```{r}
# L� os dados
classico.df <- read.table("bancos/Aula 4/cancao_do_exilio.txt")
# install.packages("stringr")
library(stringr)
# Faz o split em lista
classico.list <- str_split(classico.df$verso, pattern = " ")
# Cria as matrizes
indx <- sapply(classico.list, length)
res <- do.call(rbind, lapply(classico.list, 'length<-', max(indx)))
# Converte para data.frame
res.df <- as.data.frame(res)

```

# Separa��o de strings

� poss�vel separar uma string em v�rios "peda�os" com a fun��o str_split():

```{r}
nome_completo <- c("Lucas Ferraz Vasconcelos")
str_split(nome_completo, pattern = " ")

```

```{r}
str_split(nome_completo, pattern = "i|o")

```

Cuidado! str_split retorna uma lista! Para aninhar o output com fun��es que levam vetores como input, d� um unlist() primeiro.

Observe a diferen�a entre "" e " "

```{r}
lema <- "Ordem e progresso"
str_split(lema, pattern = "")

```

```{r}
str_split(lema, pattern = " ")

```


# Substitui��o de strings

str_replace() substitui numa string a primeira ocorr�ncia de um padr�o; str_replace_all() substitui todas as ocorr�ncias

```{r}
nome <- "J�ssica"
str_replace(nome, pattern = "s", replacement = "y")

```

Tenha sempre em mente que o R � case-sensitive!

```{r}
nome <- c("Melissa","Emmanuel","Mauro","Thamires")
str_replace_all(nome, pattern = "m", replacement = "gon")

```

# Exerc�cio A4P12

Seja "##" o n�mero de caracteres do respectivo verso do poema, crie mais uma coluna chamada "pos_modernismo" com a mensagem "Sem altera��es" caso "##" seja um m�ltiplo de 7; caso contr�rio, substitua o respectivo verso separado pelo quociente da divis�o inteira de "##" pelo n�mero do verso, em vez de espa�os I

Dica: use os operadores %% e %/%

```{r}
classico.df$numero <- str_length(classico.df$verso)
head(classico.df)
```

Obs.: bom pacote para an�lise de texto: tm (*text mining*)

```{r}
classico.df$divisao <- classico.df$numero%/%as.numeric(rownames(classico.df))
classico.df$pos_modernismo <- ifelse(classico.df$numero%%7==0, "Sem altera��o", NA)
indicesNA <- is.na(classico.df$pos_modernismo)
classico.df$pos_modernismo[indicesNA] <- 
  str_replace_all(classico.df$verso[indicesNA], pattern = ' ',
                  replacement = as.character(classico.df$divisao))
```

# Substrings

A fun��o str_sub() extrai a substring com os caracteres dentro do intervalo especificado

```{r}
palavra <- "Pindamonhangaba"
str_sub(palavra, start = 1, end = 5)
```

```{r}
str_sub(palavra, start = 8, end = 14)
```

```{r}
str_sub(palavra, start = 11, end = 11)
```

� poss�vel contar de tr�s para a frente com n�meros negativos para start e end 

Espa�os em branco tamb�m s�o levados em considera��o

```{r}
frase <- "Eu estou feliz"
str_sub(frase, start = 1, end = 5)
```

```{r}
str_sub(frase, start = 3, end = 3)
```

```{r}
str_sub(frase, start = -5, end = -1)
```

str_sub() pode ser aplicado a uma lista de strings

```{r}
ninjas <- c("Leonardo","Donatello","Michelangelo","Raphael")
str_sub(ninjas, start = 1, end = 3)
```

```{r}
str_sub(ninjas, start = 8, end = 14)
```

Caracteres solicitados que estejam fora da extens�o da string retornam como vazio

# Padroniza��o de strings

str_to_upper: converte toda a string para ma�usculas
str_to_lower: converte toda a string para min�sculas
str_to_title: converte o primeiro caracter da string para ma�uscula e o resto para min�sculas

```{r}
str_to_upper("UnB")
str_to_lower("UnB")
str_to_title("UnB")
```

Para strings compostas por v�rias palavras, a convers�o � feita uma por uma:

```{r}
str_to_upper("organiza��o DAS na��es Unidas")
str_to_lower("organiza��o DAS na��es Unidas")
str_to_title("organiza��o DAS na��es Unidas")
```

# Retirada de acentos

A seguinte sintaxe tira os acentos e c-cedilhas de uma string

```{r}
iconv("organiza��o DAS na��es Unidas", to='ASCII//TRANSLIT')
```

# Express�es regulares

Express�es regulares (regex) s�o uma linguagem que descrevem padr�es em strings

A fun��o str_view() recebe um vetor de strings e uma express�o regular e identifica onde eles batem

str_view() identifica a primeira ocorr�ncia em cada string;
str_view_all() identifica todas as ocorr�ncias

A express�o regular mais simples � a correspond�ncia exata

```{r}
# install.packages("htmlwidgets")
hierarquia <- c("Soldado","Cabo","Sargento","Tenente", "Capit�o","Major","Coronel")
str_view(hierarquia, pattern = "nt")
```

Use a barra invertida duas vezes \\ para regex de caracteres especiais

# Regex: caracteres especiais

. � um caracter especial em regex que representa qualquer caracter:

```{r}
hierarquia <- c("Soldado","Cabo","Sargento","Tenente", "Capit�o","Major","Coronel")
str_view(hierarquia, pattern = ".nt.")
```

^ representa o come�o de uma string

```{r}
hierarquia <- c("Soldado","Cabo","Sargento","Tenente", "Capit�o","Major","Coronel")
str_view(hierarquia, pattern = "^C")
```

$ representa o fim de uma string

```{r}
hierarquia <- c("Soldado","Cabo","Sargento","Tenente", "Capit�o","Major","Coronel")
str_view(hierarquia, pattern = "o$")
```

^ e $ podem ser combinadas para encontrar correspond�ncias exatas com strings inteiras:

```{r}
lista <- c("Rio Grande do Sul","Rio de Janeiro","Cabo Frio", "Tr�s Rios","Rio", "S�o Jos� do Rio Preto")
str_view(lista, pattern = "Rio")
```

^ e $ podem ser combinadas para encontrar correspond�ncias exatas com strings inteiras: footnotesize

```{r}
lista <- c("Rio Grande do Sul","Rio de Janeiro","Cabo Frio", "Tr�s Rios","Rio", "S�o Jos� do Rio Preto")
str_view(lista, pattern = "^Rio$")
```

# Regex: outros atalhos

[xyz]: qualquer um dos d�gitos "x", "y" ou "z"
[^xyz]: qualquer d�gito menos "x", "y" ou "z"

# Regex: detectando correspond�ncias

str_detect retorna um vetor l�gico que diz se a string � igual ao regex. str_count retorna o n�mero de correspond�ncias por string

* Leia o arquivo "nomesBR.rds" e o nomeie como "base". Qual a estrutura desse objeto?

```{r}
base <- readRDS("bancos/Aula 4/nomesBR.rds")
str(base)
```


* Crie duas colunas chamadas "vogais" e "consoantes" que forne�am o n�mero de vogais e consoantes em cada nome. Use dplyr::mutate

```{r}
# install.packages("dplyr")
library(dplyr)
base <- mutate(base,
               vogais = str_count(base$nome, "[AEIOU]"),
               consoantes = str_count(base$nome, "[^AEIOU]"))
```


* Crie um objeto chamado "convencional" com os nomes de mulher que come�am com vogal e que terminam com consoante

```{r}
convencional <- base[str_detect(base$nome, "^[AEIOU](.*)[^AEIOU]$") & base$sexo==2,"nome"]
head(convencional)
```

As estruturas abaixo permitem controlar quantas vezes um padr�o se repete:

?: nenhuma ou 1 vez

*: nenhuma vez ou mais

+: 1 vez ou mais

```{r}
hierarquia <- c("Soldado","Cabo","Sargento","Tenente", "Capit�o","Major","Coronel")
```


O que voc� espera que os seguintes c�digos fa�am?

str_view(hierarquia, pattern = "d.+")

str_view(hierarquia, pattern = ".o*")

str_view(hierarquia, pattern = "it?")

```{r}
str_view(hierarquia, pattern = "d.+")
str_view(hierarquia, pattern = ".o*")
str_view(hierarquia, pattern = "it?")
```

