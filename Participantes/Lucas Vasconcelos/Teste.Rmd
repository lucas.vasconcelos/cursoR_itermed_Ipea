---
title: "Teste"
author: "Lucas Vasconcelos"
date: "28 de setembro de 2017"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r, warning=FALSE, message=FALSE}
library(dplyr)
library(knitr)
data(iris)
iris %>% 
  dplyr::select(-c(3,4)) %>% 
  dplyr::group_by(Species) %>% 
  dplyr::mutate(Sepal.Ratio = Sepal.Length/Sepal.Width, Log.Sepal = log(Sepal.Length)) %>% 
  dplyr::filter(Sepal.Ratio > 1.85 & Sepal.Ratio < 2) %>% 
  dplyr::summarise(n.long.Sepal = n(), Assimetria = psych::skew(Sepal.Length)) %>%
  knitr::kable()
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
